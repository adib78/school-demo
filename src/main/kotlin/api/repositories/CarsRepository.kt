package api.repositories

import org.springframework.data.jpa.repository.JpaRepository
import api.entities.CarEntity

interface CarsRepository : JpaRepository<CarEntity, Long>
