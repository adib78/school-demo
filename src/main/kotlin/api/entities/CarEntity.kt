package api.entities

import java.io.Serializable
import java.time.LocalDateTime
import javax.persistence.*

@Entity(name = "cars")
data class CarEntity(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long = 0,

    @Column(columnDefinition = "TEXT")
    var name: String = "",

    @Column
    var productionDate: LocalDateTime = LocalDateTime.now()

) : Serializable