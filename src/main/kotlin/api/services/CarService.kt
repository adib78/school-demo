package api.services

import api.repositories.CarsRepository
import org.springframework.stereotype.Service

@Service
class CarService(
        private val carsRepository: CarsRepository
) {
  fun getCars(): List<String> {
      return carsRepository.findAll()
              .map{ it.name }
//              .sorted()
  }

}
