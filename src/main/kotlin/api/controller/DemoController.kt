package api.controller

import api.services.CarService
import org.springframework.web.bind.annotation.*

@CrossOrigin()
@RestController
@RequestMapping("/api")
class DemoController(
        val carService: CarService
) {
    @GetMapping("/cars")
    fun getCars1(): List<String> = carService.getCars()
}

