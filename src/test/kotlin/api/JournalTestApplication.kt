package api

import org.springframework.boot.runApplication
import api.Application

fun main(args: Array<String>) {
	runApplication<Application>(*args)
}
