insert into cars(id, name, production_date) values
  (1, 'Ferrari', CURRENT_TIMESTAMP()),
  (2, 'Audi', CURRENT_TIMESTAMP()),
  (3, 'Mercedes', CURRENT_TIMESTAMP()),
  (4, 'Skoda', CURRENT_TIMESTAMP()),
  (5, 'Fiat', CURRENT_TIMESTAMP()),
  (6, 'Łada', CURRENT_TIMESTAMP());
