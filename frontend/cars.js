let dataList = document.getElementById("json-datalist");
let input = document.getElementById("ajax");

let url = "http://localhost:9999/api/cars";
getDataListFromDatabase(url);
function getDataListFromDatabase(url) {
    fetch(url)
        .then(response => response.json())
        .then( dropdownList => {
            dataList.options.length = 0;
            dropdownList.forEach(function(item) {
                let option = document.createElement("option");
                option.value = item;
                dataList.appendChild(option);
            });
            input.placeholder = "e.g. car";
        })
}

